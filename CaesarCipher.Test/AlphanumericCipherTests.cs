﻿using CaesarCipher.Core;
using System;
using Xunit;

namespace CaesarCipher.Test
{
    public class AlphanumericCipherTests
    {
        [Theory]
        [InlineData(5, "hello", "mjqqt")]
        [InlineData(12, "Congratulations", "Oazsdmfgxmfuaze")]
        [InlineData(12, "Congratulations12345", "Oazsdmfgxmfuaze34567")]
        public void EncodingTest(int shift, string input, string result)
        {
            var cipher = new AlphanumericCipher(shift);
            var encoded = cipher.Encode(input);
            Assert.Equal(result, encoded);
        }

        [Theory]
        [InlineData(5, "mjqqt", "hello")]
        [InlineData(12, "Oazsdmfgxmfuaze", "Congratulations")]
        [InlineData(12, "Oazsdmfgxmfuaze34567", "Congratulations12345")]
        public void DecodingTest(int shift, string input, string result)
        {
            var cipher = new AlphanumericCipher(shift);
            var decoded = cipher.Decode(input);
            Assert.Equal(result, decoded);
        }
    }
}
