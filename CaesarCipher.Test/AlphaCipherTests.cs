﻿using CaesarCipher.Core;
using System;
using Xunit;

namespace CaesarCipher.Test
{
    public class AlphaCipherTests
    {
        [Theory]
        [InlineData(5, "hello", "mjqqt")]
        [InlineData(12, "Congratulations", "Oazsdmfgxmfuaze")]
        [InlineData(12, "Congratulations12345", "Oazsdmfgxmfuaze12345")]
        public void EncodingTest(int shift, string input, string result)
        {
            var cipher = new AlphaCipher(shift);
            var encoded = cipher.Encode(input);
            Assert.Equal(result, encoded);
        }

        [Theory]
        [InlineData(5, "mjqqt", "hello")]
        [InlineData(12, "Oazsdmfgxmfuaze", "Congratulations")]
        [InlineData(12, "Oazsdmfgxmfuaze12345", "Congratulations12345")]
        public void DecodingTest(int shift, string input, string result)
        {
            var cipher = new AlphaCipher(shift);
            var decoded = cipher.Decode(input);
            Assert.Equal(result, decoded);
        }
    }
}
