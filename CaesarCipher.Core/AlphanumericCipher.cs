﻿using System;
using System.Text;

namespace CaesarCipher.Core
{
    public class AlphanumericCipher
    {
        private readonly int _shiftValue;

        public AlphanumericCipher(int shiftValue)
        {
            _shiftValue = shiftValue;
        }

        public string Encode(string value)
        {
            var builder = new StringBuilder(value.Length);
            foreach (var symbol in value)
            {
                if (symbol >= 65 && symbol <= 90)
                {
                    int convertedValue = symbol - 65;
                    convertedValue = (convertedValue + _shiftValue) % 26;
                    char encodedSymbol = (char)(convertedValue + 65);
                    builder.Append(encodedSymbol);
                }
                else if (symbol >= 97 && symbol <= 122)
                {
                    int convertedValue = symbol - 97;
                    convertedValue = (convertedValue + _shiftValue) % 26;
                    char encodedSymbol = (char)(convertedValue + 97);
                    builder.Append(encodedSymbol);
                }
                else if (symbol >= 48 && symbol <= 57)
                {
                    int convertedValue = symbol - 48;
                    convertedValue = (convertedValue + _shiftValue) % 10;
                    char encodedSymbol = (char)(convertedValue + 48);
                    builder.Append(encodedSymbol);
                }
                else
                {
                    builder.Append(symbol);
                }
            }

            return builder.ToString();
        }

        public string Decode(string value)
        {
            var builder = new StringBuilder(value.Length);
            foreach (var symbol in value)
            {
                if (symbol >= 65 && symbol <= 90)
                {
                    int convertedValue = symbol - 65;
                    convertedValue -= _shiftValue;
                    if (convertedValue < 0)
                        convertedValue += 26;

                    char encodedSymbol = (char)(convertedValue + 65);
                    builder.Append(encodedSymbol);
                }
                else if (symbol >= 97 && symbol <= 122)
                {
                    int convertedValue = symbol - 97;
                    convertedValue -= _shiftValue;
                    if (convertedValue < 0)
                        convertedValue += 26;

                    char encodedSymbol = (char)(convertedValue + 97);
                    builder.Append(encodedSymbol);
                }
                else if (symbol >= 48 && symbol <= 57)
                {
                    int convertedValue = symbol - 48;
                    convertedValue -= _shiftValue;
                    if (convertedValue < 0)
                        convertedValue += 10;

                    char encodedSymbol = (char)(convertedValue + 48);
                    builder.Append(encodedSymbol);
                }
                else
                {
                    builder.Append(symbol);
                }
            }

            return builder.ToString();
        }
    }
}
